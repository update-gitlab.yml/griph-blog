---
title: "Whats This?"
url: "/about/"
summary: about
---

This is a blog, but you already knew that.

This will be a place for me to keep track of my personal projects, mostly server and infrastructure stuff, but there
might be some code.

The tone on here will vary from rambling and conversational to a bit more polished for guides.

## Why?

As I work through the difficulties of wrangling servers, deploying services, and writing code. it's nice to have a place
to store the steps taken and problems I encountered so that if it comes up again I can refer back to what I did last
time. Plus if I can't find a solution online and have to fix some obscure problem myself, I'll post the fix here in the
hope that I can save someone the hours it no doubt took me to fix.

Also, when you have a kubernetes cluster you need something to host on it...

## About Me

My name is Kieran (or Griph_ you decide) just be reassured that there is a real person behind the blog, and not some
complicated text generation AI...

Computer Science graduate, but mostly a software developer now. I program mainly in Java and Python, and I am trying to
learn Kotlin. I also have some leftover Uni knowledge of C/C++ and Haskell - which may show up from time to time.
Haskell is *cool*.

My interests apart from servers and programming that will show up here do extend to gaming, record collecting, and
tinkering on cars.

## Thanks to

Thanks to everyone who works on hugo! a brilliant bit of tech which means I can write markdown and have it converted
for me into a beautiful blog! without it writing a blog would have taken much longer, and I would not have been able to
avoid JavaScript, or worse... PHP.  

The links to Hugo and the theme PaperMod can be found in the footer of every page! 👇
