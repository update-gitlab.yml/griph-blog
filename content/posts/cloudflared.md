---
title: "Homelab Ingress"
date: "2022-09-28"
draft: true
author: "Kieran Dennis"
tags: ["k8s", "networking", "cloudflare"]
---

It's been a while since I made a blog post, And I've just finished up a (relatively) quick switch over in my homelab
network layout for inbound traffic to kubernetes - A perfect time to make a post about it! 

It's always a problem hosting services at home, Because there's a risk of exposing your home network to all sorts of nasty
attacks, not to mention the amount of detail an IP address can reveal. So I've always been looking for ways to lock this
down. In this post I'll be going over how I currently manage the inbound traffic, also known as ingress, into my homelab.
I'll also summarise a few false starts along the way to what is (currently) a solid solution.

# Introduction & Background

My first thoughts were to use a virtual private server, with a reverse proxy and a VPN connection, aside from the fact
that it defeats the purpose of a *homelab*, this brings additional cost, along with a whole pile of maintenance. 
It also doesn't exactly guarantee security as the VPS is still a large attack surface, and would need a firewall and
at minimum basic DDoS protection to consider.

While looking for DDoS protection [Cloudflare](https://www.cloudflare.com) came up, Their proxying service points a DNS 
record (blog.griph.co.uk in this case) at cloudflare first, and then proxies the HTTP traffic to your local IP address.
This also completely does away with the need for a VPS as cloudflare hides your IP behind a TCP proxy.
I used this method with fairly little issue for a few months.

//Diagram here of DNS -> CF Proxy -> Router -> RPI NGINX -> K8s

The main pain points with this setup is the need for a static IP address, which on a domestic broadband provider can be
quite tricky to get hold of. Because of this I had to use a service to update the IP address in cloudflare via their API
each time my ISP decided to give me a new public IP. Also, it still needs a proxy within the local network. On a 
self-hosted Kubernetes cluster Ingress is also not a trivial task like in the large cloud providers. 
[MetalLB](https://metallb.universe.tf/) goes a long way to help this, providing LAN-level IP addresses for services 
outside the cluster, bit this still required me to manually update my nginx gateway with new services each time.

After getting bored with dealing with SSL certs between my gateway NGINX and cloudflare, I decided that a VPN might not 
be so bad after all - As look would have it however, Cloudflare have their own solution for this,
[Cloudflare Tunnel](https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/) Which handles all the 
complex connection and config around the VPN setup, without needing a VPS. This also has the added advantage of not needing
any open ports on my home network - plugging up security holes.

//Diagram here of DNS -> CF Proxy -> TUNNEL -> Cloudflared -> K8s

Cloudflare routes traffic through a VPN tunnel to the locally running Cloudflared service, which then forwards this on
to local network services. Configuration can be done either locally in the service, or remotely through the zero-trust 
dashboard.

For the last year or so, the cloudflare tunnel service Cloudflared has been ticking away nicely on my raspberry pi 4, 
having given up its duties as an NGINX proxy. This did however still leave one problem - Redundancy. My 3 node K8s cluster
has built in fail-over and resilience, But if the pi needs updates or restarting then **ALL**  of my services are down - as
the tunnel is not there to receive traffic.

The more astute readers may have already worked out the next step, just run Cloudflared *in* the cluster! The rest of
this post will be a summary of setting up cloudflare tunnel from scratch. This information is taken from a number of 
different pages in Cloudflare's well detailed documentation. Just with a bit of additional configuration for my use case.
The bulk of this can be found [here](https://developers.cloudflare.com/cloudflare-one/tutorials/many-cfd-one-tunnel/).

# Cloudflare Tunnels

## Install Cloudflared

## Generate Credentials

# Deploy to Kubernetes

## Configuring Cloudflared

### configMap

### DaemonSet