---
title: "Hardware overview: Dell Micro"
date: 2021-05-20
draft: true
cover:  
  image: "/img/dell-micro/cover.JPG"
  caption: "The Dell Micro 3060"
---

In this post I'll be going over the backbone of my homelab, The Dell Micro 3060. This unsuspecting ultra-small form
factor business PC packs a surprising punch, and although I'm certainly not the first person to say it, these machines
are a great choice for a low-power, and low-noise server.

I have to start by acknowledging Patrick over at Serve The Home for the brilliant 
[TinyMiniMicro series](https://www.servethehome.com/introducing-project-tinyminimicro-home-lab-revolution/), which was 
the inspiration for the start of my homelab. So for the remainder of this post I will be standing on the shoulders of 
giants.

## Where this all started
For a long time I've been interested in self-hosting. Like most people I got my first fix hosting minecraft servers for 
a few friends. The technology that underpins this hosting has changed over the years, but fundamentally the reasons are 
still the same. 


![back](/img/dell-micro/back.JPG)


![inside](/img/dell-micro/inside.JPG)


![inside_no_ssd](/img/dell-micro/inside_no_ssd.JPG)


![SSD](/img/dell-micro/SSD.JPG)


![cpu_old_paste](/img/dell-micro/cpu_old_paste.JPG)


![heatsink](/img/dell-micro/heatsink.JPG)


![clean_cpu](/img/dell-micro/clean_cpu.JPG)